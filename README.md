## 开发环境


> JDK 1.8
>
> MySQL 5.7
>
> MAVEN 3.8.1
>
> Tomcat 9.0.45 
>
> SpringMVC

引用 jar 包：

junit、druid、dbutils、dom4j、mysql-connector、servlet、fastjson、jackson-databind、jackson-core、jackson-annotations、lombok、aspectjweaver、spring-webmvc、spring-core、jwt



------



## 演示



### 登录页面
![image-20210623163050601](https://img-blog.csdnimg.cn/img_convert/42e82dcea7505b54b1cfef91b1f73a24.png)






### 学生页面

![image-20210623155428818](https://img-blog.csdnimg.cn/img_convert/d3905a4e47a5a672a3a045f614dfd2bc.png)





### 教师页面
![image-20210623155330552](https://img-blog.csdnimg.cn/img_convert/0cf9f7fa77d0cd0383c8fccfb4b184f2.png)
![image-20210623155545861](https://img-blog.csdnimg.cn/img_convert/95053d9aac75a310e3422fdc79376243.png)





### 管理员页面

![image-20210623155722671](https://img-blog.csdnimg.cn/img_convert/0e8d10c598ab41f0c453a7d0bff33556.png)











------



## 系统概述



学生成绩信息管理系统涉及到学生、教师、系统管理员、班级、学生成绩、课程。

- 管理员通过账号、密码登录，管理员进入系统可以对课程，系部，教师，学生，专业进行添加、查看。
- 教师通过教工号和密码登录，教师进入系统可以查看自己教的所有课程，以及对选该课程的学生成绩进行修改。

- 学生通过学号和密码进行登录，学生进入系统可以查看自己学习课程的信息以及成绩。



------



## 系统功能分析



Student

1. 登录
2. 查看成绩



------



Teacher

1. 登录
2. 查看课程
3. 查看学生成绩
4. 录入成绩



------



Administrator

1. 登录
2. 查看系部
3. 查看系部下的专业
4. 查看专业下的班级
5. 查看班级下的学生
6. 新建系部
7. 新建系部下的专业
8. 新建专业下的班级
9. 新建班级下的学生
10. 新建课程



