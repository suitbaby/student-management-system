/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.34-log : Database - sams
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`sams` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `sams`;

/*Table structure for table `administrators` */

DROP TABLE IF EXISTS `administrators`;

CREATE TABLE `administrators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `account` varchar(40) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`account`) COMMENT '账号唯一'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `administrators` */

insert  into `administrators`(`id`,`name`,`account`,`password`) values 
(1,'超级管理员','admin','admin');

/*Table structure for table `classes` */

DROP TABLE IF EXISTS `classes`;

CREATE TABLE `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `graduationyear` varchar(40) DEFAULT NULL,
  `majorid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `majorid` (`majorid`),
  CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`majorid`) REFERENCES `majors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `classes` */

/*Table structure for table `courses` */

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `teacherid` int(11) DEFAULT NULL,
  `beginyear` timestamp NULL DEFAULT NULL,
  `endyear` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teacherid` (`teacherid`),
  CONSTRAINT `courses_ibfk_1` FOREIGN KEY (`teacherid`) REFERENCES `teachers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `courses` */

/*Table structure for table `majors` */

DROP TABLE IF EXISTS `majors`;

CREATE TABLE `majors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `sdeptid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sdeptid` (`sdeptid`),
  CONSTRAINT `majors_ibfk_1` FOREIGN KEY (`sdeptid`) REFERENCES `sdepts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `majors` */

/*Table structure for table `scores` */

DROP TABLE IF EXISTS `scores`;

CREATE TABLE `scores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `studentid` int(11) DEFAULT NULL,
  `courseid` int(11) DEFAULT NULL,
  `grade` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `studentid` (`studentid`,`courseid`),
  KEY `courseid` (`courseid`),
  CONSTRAINT `scores_ibfk_1` FOREIGN KEY (`studentid`) REFERENCES `students` (`id`),
  CONSTRAINT `scores_ibfk_2` FOREIGN KEY (`courseid`) REFERENCES `courses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/*Data for the table `scores` */

/*Table structure for table `sdepts` */

DROP TABLE IF EXISTS `sdepts`;

CREATE TABLE `sdepts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `sdepts` */

/*Table structure for table `students` */

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `sdeptid` int(11) DEFAULT NULL,
  `majorid` int(11) DEFAULT NULL,
  `classid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sdeptid` (`sdeptid`),
  KEY `majorid` (`majorid`),
  KEY `classid` (`classid`),
  CONSTRAINT `students_ibfk_1` FOREIGN KEY (`sdeptid`) REFERENCES `sdepts` (`id`),
  CONSTRAINT `students_ibfk_2` FOREIGN KEY (`majorid`) REFERENCES `majors` (`id`),
  CONSTRAINT `students_ibfk_3` FOREIGN KEY (`classid`) REFERENCES `classes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2020020223 DEFAULT CHARSET=utf8;

/*Data for the table `students` */

/*Table structure for table `teachers` */

DROP TABLE IF EXISTS `teachers`;

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `sdeptid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sdeptid` (`sdeptid`),
  CONSTRAINT `teachers_ibfk_1` FOREIGN KEY (`sdeptid`) REFERENCES `sdepts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2020221 DEFAULT CHARSET=utf8;

/*Data for the table `teachers` */

/*Table structure for table `tokens` */

DROP TABLE IF EXISTS `tokens`;

CREATE TABLE `tokens` (
  `token` varchar(120) NOT NULL,
  `overtime` timestamp NULL DEFAULT NULL,
  `adminId` int(11) DEFAULT NULL,
  `studentId` int(11) DEFAULT NULL,
  `teacherId` int(11) DEFAULT NULL,
  PRIMARY KEY (`token`),
  KEY `adminId` (`adminId`),
  KEY `studentId` (`studentId`),
  KEY `teacherId` (`teacherId`),
  CONSTRAINT `tokens_ibfk_1` FOREIGN KEY (`adminId`) REFERENCES `administrators` (`id`),
  CONSTRAINT `tokens_ibfk_2` FOREIGN KEY (`studentId`) REFERENCES `students` (`id`),
  CONSTRAINT `tokens_ibfk_3` FOREIGN KEY (`teacherId`) REFERENCES `teachers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tokens` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
