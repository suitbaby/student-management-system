package utils;

import org.springframework.util.DigestUtils;
import pojo.Tokens;

/**
 * 验证是否登录
 */
public class TokenUtils {

    //设置过期时间30分钟
    private static final long EXPIRE_DATE=30*60*10000;

    public static Long overTime(long time){
        return time+TokenUtils.EXPIRE_DATE;
    }

    /**
     * 通过id，和当前时间加密
     * @param id id
     * @param time 当前时间
     * @return 加密之后的字符串
     */
    public static String encryption(String id,long time ){
        return DigestUtils.md5DigestAsHex((id+time).getBytes());
    }


    /**
     * 判断是否过期
     * @param token tokens对象
     * @return 返回true生效,false过期
     */
    public static Boolean decrypt(Tokens token){

        return token.getOverTime().getTime() >= System.currentTimeMillis();
    }



}
