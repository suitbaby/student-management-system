package utils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JSONUtils {

    public static void responseJson(HttpServletResponse response, String jsonText) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(jsonText);
    }


}
