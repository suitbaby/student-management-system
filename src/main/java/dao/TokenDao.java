package dao;

import pojo.Tokens;

public interface TokenDao {

    /**
     * 保存token令牌
     *
     * @param token token令牌
     * @return 如果返回-1,说明失败，反之亦然
     */
    int saveToken(Tokens token);

    /**
     * 通过token获取Token信息
     *
     * @param token token令牌
     * @return 如果返回null, 则获取失败，反之亦然
     */
    Tokens queryByToken(String token);

    /**
     * 根据token删除
     *
     * @param token token字段
     * @return 返回-1删除失败，其他表示成功
     */
    int deleteByToken(String token);

    /**
     * 根据学生学号删除
     *
     * @param id 学生Id
     * @return 返回-1表示删除失败，返回其他表示删除成功
     */
    int deleteByStudentId(String id);

    /**
     * 根据教工号删除
     *
     * @param id 教工号
     * @return 返回-1表示删除失败，返回其他表示删除成功
     */
    int deleteByTeacherId(String id);

    /**
     * 根据管理员id 删除
     *
     * @param id admin id
     * @return 返回-1表示删除失败，返回其他表示删除成功
     */
    int deleteByAdminId(String id);


}
