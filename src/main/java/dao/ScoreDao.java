package dao;

import pojo.Scores;

import java.util.List;

public interface ScoreDao {

    /**
     * 根据学生学号查询学生所有课程成绩信息
     * @param studentId 学生学号
     * @return 如果返回null,说明这个学生没有选课，反之亦然
     */
    List<Scores> queryScoreByStudentId(String studentId);


    /**
     * 根据学生学号和课程号查询单门课程成绩
     * @param studentId 学生学号
     * @param courseId 课程号
     * @return 如果返回null,说明没有这个学生，反之亦然
     */
    Scores queryScoreByStudentIdAndCourseId(String studentId, String courseId);


    /**
     * 保存学生成绩
     * @param score 成绩对象
     * @return 如果返回-1,说明失败，反之亦然
     */
    int saveScore(Scores score);


    /**
     * 获取所有课程的成绩
     * @return 各科成绩List集合
     */
    List<Scores> queryAllScore();

    /**
     * 获取学生的各科平均成绩
     * @param studentId 学生Id
     * @return 平均成绩
     */
    Double queryStudentScoreAVGByStudentId(String studentId);

    /**
     * 根据课程号查询成绩
     * @param courseId 课程号
     * @return 学习该课程的所有学生
     */
    List<Scores> queryScoreByCourseId(String courseId);


    /**
     * 根据studentId和courseId修改课程成绩
     * @param score 课程对象
     * @return 返回-1修改失败，返回其他修改成功
     */
    int updateGradeByStudentIdAndCourseId(Scores score);


}
