package dao.impl;

import dao.CourseDao;
import pojo.Courses;
import java.util.List;

public class CourseDaoImpl extends BaseDao implements CourseDao {

    @Override
    public Courses queryCourseById(String id) {
        String sql = "SELECT id,NAME,teacherId,beginYear beginDate,endYear endDate FROM courses WHERE id =?";
        return queryForOne(Courses.class,sql,id);
    }

    @Override
    public List<Courses> queryCourseByTeacherId(String id) {
        String sql ="SELECT id,NAME,teacherId,beginYear beginDate,endYear endDate FROM courses WHERE teacherId =?";
        return queryForList(Courses.class,sql,id);
    }

    @Override
    public List<Courses> queryAllCourse() {
        String sql ="SELECT id,NAME,teacherId,beginYear beginDate,endYear endDate FROM courses";
        return queryForList(Courses.class,sql);
    }

    @Override
    public int saveCourse(Courses courses) {
        String sql = "INSERT INTO courses(NAME,teacherId,beginYear,endYear) VALUES(?,?,?,?)";
        return update(sql,courses.getName(),courses.getTeacherId(),courses.getBeginDate(),courses.getEndDate());
    }

    @Override
    public String queryNameById(String id) {
        String sql = "SELECT NAME FROM courses WHERE id=? ";
        return (String) queryForSingleValue(sql,id);
    }

    @Override
    public Courses queryByNameAndTeacherId(Courses courses) {
        String sql = "SELECT id,NAME,teacherId,beginYear beginDate FROM courses WHERE  NAME=? AND teacherId=?";
        return queryForOne(Courses.class,sql,courses.getName(),courses.getTeacherId());
    }


}
