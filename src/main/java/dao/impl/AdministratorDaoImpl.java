package dao.impl;

import dao.AdministratorDao;
import pojo.Administrators;


public class AdministratorDaoImpl extends BaseDao implements AdministratorDao {

    @Override
    public Administrators queryAdministratorByAccountAndPassword(String account, String password) {
        String sql = "select id,name,account,password from administrators where account=? and password = ?";
        return queryForOne(Administrators.class,sql,account,password);
    }

    @Override
    public Administrators queryAdministratorByAccount(String account) {
        String sql = "select id,name,account,password from administrators where account=?";
        return queryForOne(Administrators.class,sql,account);
    }

    @Override
    public int saveAdministrator(Administrators administrator) {
        String sql="insert into administrators(name,account,password) values(?,?,?)";
        return update(sql,administrator.getName(),administrator.getAccount(),administrator.getPassword());
    }


}
