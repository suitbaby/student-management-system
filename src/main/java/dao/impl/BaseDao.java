package dao.impl;


import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import utils.JDBCUtils;
import java.sql.*;
import java.util.List;


/**
 * 操作数据
 */
public abstract class BaseDao {

    private QueryRunner runner = new QueryRunner();

    /**
     * update()方法用来执行Insert/Update/Delete语句
     *
     * @param sql sql语句
     * @param args 参数
     * @return 如果返回-1，则说明执行失败，<br/>返回其他说明影响的行数
     */
    public int update(String sql, Object... args) {
        Connection connection = JDBCUtils.getConnection();
        try {
            return runner.execute(connection, sql, args);
        } catch (SQLException e) {
           e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(connection);
        }
        return -1;
    }



    /**
     * 查询返回一个JavaBean的sql语句
     *
     * @param clazz 返回的对象类型
     * @param sql   执行的sql语句
     * @param args  sql对应的参数
     * @param <T>   返回类型的泛型
     * @return 返回一个对象
     */
    public <T> T queryForOne(Class<T> clazz, String sql, Object... args) {
        Connection connection = JDBCUtils.getConnection();
        try {
            return runner.query(connection, sql, new BeanHandler<>(clazz), args);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(connection);
        }
        return null;
    }

    /**
     * 查询返回多个JavaBean的sql语句
     *
     * @param clazz 返回的对象类型
     * @param sql   执行的sql语句
     * @param args  sql对应的参数
     * @param <T>   返回类型的泛型
     * @return 返回多个对象
     */
    public <T> List<T> queryForList(Class<T> clazz, String sql, Object... args) {
        Connection connection = JDBCUtils.getConnection();
        try {
            return runner.query(connection, sql, new BeanListHandler<>(clazz), args);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(connection);
        }
        return null;
    }

    /**
     * 执行返回一行一列的sql语句
     *
     * @param sql  执行的sql
     * @param args sql对应的参数值
     * @return 返回一个一行一列的值
     */
    public Object queryForSingleValue(String sql, Object... args) {
        Connection connection = JDBCUtils.getConnection();
        try {
            return runner.query(connection, sql, new ScalarHandler(), args);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DbUtils.closeQuietly(connection);
        }
        return null;
    }
}
