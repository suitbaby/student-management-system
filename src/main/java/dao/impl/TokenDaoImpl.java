package dao.impl;

import dao.TokenDao;
import pojo.Tokens;

public class TokenDaoImpl extends BaseDao implements TokenDao {

    @Override
    public int saveToken(Tokens token) {
        String sql ="INSERT INTO tokens(token,overtime,adminId,studentId,teacherId) VALUES(?,?,?,?,?)";
        return update(sql,token.getToken(),token.getOverTime(),token.getAdminId(),token.getStudentId(),token.getTeacherId());
    }

    @Override
    public Tokens queryByToken(String token) {
        String sql = "SELECT token,overtime,adminId,studentId,teacherId FROM tokens WHERE token =?";
        return queryForOne(Tokens.class,sql,token);
    }

    @Override
    public int deleteByToken(String token) {
        String sql ="DELETE FROM tokens WHERE token=?";
        return update(sql,token);
    }

    @Override
    public int deleteByStudentId(String id) {
        String sql ="DELETE FROM tokens WHERE studentId=?";
        return update(sql,id);
    }

    @Override
    public int deleteByTeacherId(String id) {
        String sql ="DELETE FROM tokens WHERE teacherId=?";
        return update(sql,id);
    }

    @Override
    public int deleteByAdminId(String id) {
        String sql ="DELETE FROM tokens WHERE adminId=?";
        return update(sql,id);
    }
}
