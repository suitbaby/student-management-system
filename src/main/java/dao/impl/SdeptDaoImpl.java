package dao.impl;

import dao.SdeptDao;
import pojo.Sdepts;
import java.util.List;

public class SdeptDaoImpl extends BaseDao implements SdeptDao {

    @Override
    public Sdepts querySdeptById(String id) {
        String sql = "SELECT id,NAME FROM sdepts WHERE id=?";
        return queryForOne(Sdepts.class,sql,id);
    }

    @Override
    public Sdepts querySdeptByName(String name) {
        String sql="SELECT id,NAME FROM sdepts WHERE NAME=?";
        return queryForOne(Sdepts.class,sql,name);
    }

    @Override
    public Sdepts querySdeptByIdAndName(String id, String name) {
        String sql = "SELECT id,NAME FROM sdepts WHERE NAME=? AND id=?";
        return queryForOne(Sdepts.class,sql,name,id);
    }

    @Override
    public int saveSdept(Sdepts sdept) {
        String sql = "INSERT INTO sdepts(id,NAME) VALUES(?,?) ";
        return update(sql,sdept.getId(),sdept.getName());
    }

    @Override
    public List<Sdepts> queryAllSdept() {
        String sql = "SELECT id,NAME FROM sdepts";
        return queryForList(Sdepts.class,sql);
    }

}
