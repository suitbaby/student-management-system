package dao.impl;

import dao.MajorDao;
import pojo.Majors;
import java.util.List;

public class MajorDaoImpl extends BaseDao implements MajorDao {
    @Override
    public Majors queryMajorById(String id) {
        String sql = "SELECT id,NAME,sdeptid FROM majors WHERE id = ?";
        return queryForOne(Majors.class,sql,id);
    }

    @Override
    public List<Majors> queryMajorBySdeptId(String id) {
        String sql = "SELECT id,NAME,sdeptid FROM majors WHERE sdeptid=?";
        return queryForList(Majors.class,sql,id);
    }

    @Override
    public List<Majors> queryMajorByName(String name) {
        String sql = "SELECT id,NAME,sdeptid FROM majors WHERE name=?";
        return queryForList(Majors.class,sql,name);
    }

    @Override
    public Majors queryMajorByIdAndName(String id, String name) {
        String sql = "SELECT id,NAME,sdeptid FROM majors WHERE id =? and name =?";
        return queryForOne(Majors.class,sql,id,name);
    }

    @Override
    public int saveMajor(Majors major) {
        String sql = "INSERT INTO majors(NAME,sdeptid) VALUES(?,?)";
        return update(sql,major.getName(),major.getSdeptId());
    }

    @Override
    public List<Majors> queryAllMajor() {
        String sql = "SELECT id,NAME,sdeptid FROM majors";
        return queryForList(Majors.class,sql);
    }
}
