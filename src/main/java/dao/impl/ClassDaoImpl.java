package dao.impl;

import dao.ClassDao;
import pojo.Classes;
import java.util.List;

public class ClassDaoImpl extends BaseDao implements ClassDao {

    @Override
    public Classes queryById(String id) {
        String sql = "SELECT id,NAME,graduationYear,majorId FROM classes WHERE id =?";
        return queryForOne(Classes.class, sql, id);
    }

    @Override
    public List<Classes> queryClassesByMajorId(String id) {
        String sql = "SELECT id,NAME,graduationYear,majorId FROM classes WHERE majorId =?";
        return queryForList(Classes.class, sql, id);
    }

    @Override
    public List<Classes> queryClassesByName(String name) {
        String sql = "SELECT id,NAME,graduationYear,majorId FROM classes WHERE NAME = ?";
        return queryForList(Classes.class, sql, name);
    }

    @Override
    public Classes queryClassesByIdAndName(String id, String name) {
        String sql = "SELECT id,NAME,graduationYear,majorId FROM classes WHERE id = ? AND NAME = ?";
        return queryForOne(Classes.class, sql, id, name);
    }

    @Override
    public int saveClasses(Classes classes) {
        String sql = "INSERT INTO classes(NAME,graduationYear,majorId) VALUES(?,?,?)";
        return update(sql,classes.getName(),classes.getGraduationYear(),classes.getMajorId());
    }

    @Override
    public List<Classes> queryAllClasses() {
        String sql="SELECT id,NAME,graduationYear,majorId FROM classes";
        return queryForList(Classes.class,sql);
    }
}
