package dao.impl;

import dao.TeacherDao;
import pojo.Teachers;
import java.util.List;

public class TeacherDaoImpl extends BaseDao implements TeacherDao {

    @Override
    public Teachers queryTeacherByIdAndPassword(String id, String password) {
        String sql = "SELECT id,NAME,PASSWORD,sdeptid FROM teachers WHERE id=? AND PASSWORD = ?";
        return queryForOne(Teachers.class,sql,id,password);
    }

    @Override
    public Teachers queryTeacherById(String id) {
        String sql = "SELECT id,NAME,sdeptid FROM teachers WHERE id=?";
        return queryForOne(Teachers.class,sql,id);
    }

    @Override
    public int saveTeacher(Teachers teacher) {
        String sql = "INSERT INTO teachers(id,NAME,PASSWORD,sdeptid) VALUES(?,?,?,?)";
        return update(sql,teacher.getId(),teacher.getName(),teacher.getPassword(),teacher.getSdeptId());
    }

    @Override
    public List<Teachers> queryAllTeacher() {
        String sql ="SELECT id,NAME,sdeptid FROM teachers";
        return queryForList(Teachers.class,sql);
    }

    @Override
    public List<Teachers> queryTeacherBySdeptId(String sdeptid) {
        String sql = "SELECT id,NAME,sdeptid FROM teachers WHERE sdeptid = ?";
        return queryForList(Teachers.class,sql,sdeptid);
    }

    @Override
    public int queryTeacherNumber() {
        String sql = "SELECT COUNT(*) FROM teachers";
        return Integer.parseInt(queryForSingleValue(sql)+"");
    }

    @Override
    public int queryTeacherNumberBySdeptId(String sdeptid) {
        String sql = "SELECT COUNT(*) FROM teachers where sdeptid = ?";
        return Integer.parseInt(queryForSingleValue(sql,sdeptid)+"");
    }


}
