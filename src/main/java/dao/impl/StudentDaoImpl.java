package dao.impl;

import dao.StudentDao;
import pojo.Students;
import java.util.List;

public class StudentDaoImpl extends BaseDao implements StudentDao {

    @Override
    public Students queryStudentById(String id) {
        String sql = "select id,name,sdeptId,majorId,classId from students where id =?";
        return queryForOne(Students.class, sql, id);
    }

    @Override
    public String queryNameById(String id) {
        String sql = "SELECT NAME FROM students WHERE id =?";
        return (String) queryForSingleValue(sql,id);
    }

    @Override
    public Students queryStudentByIdAndPassword(String id, String password) {
        String sql = "select id,name,password,sdeptId,majorId,classId from students where id =? and password =?";
        return queryForOne(Students.class, sql, id,password);
    }

    @Override
    public int saveStudent(Students student) {
        String sql ="insert into students(id,name,password,sdeptId,majorId,classId) values(?,?,?,?,?,?)";
        return update(sql,student.getId(),student.getName(),student.getPassword(),student.getSdeptId(),student.getMajorId(),student.getClassId());
    }

    @Override
    public List<Students> queryAllStudent() {
        String sql = "SELECT id,NAME,sdeptId,majorId,classId FROM students";
        return queryForList(Students.class,sql);
    }

    @Override
    public List<Students> queryStudentByClassId(String classId) {
        String sql = "SELECT id,NAME,sdeptId,majorId,classId FROM students where classId = ?";
        return queryForList(Students.class,sql,classId);
    }


}
