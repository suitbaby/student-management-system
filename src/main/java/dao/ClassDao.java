package dao;

import pojo.Classes;

import java.util.List;

public interface ClassDao {


    /**
     * 根据班级id查询班级
     * @param id 班级id
     * @return 如果返回null,说明没有这个班级，反之亦然
     */
    Classes queryById(String id);

    /**
     * 根据专业id查询班级
     * @param id 专业id
     * @return 如果返回null,说明专业下没有班级，反之亦然
     */
    List<Classes> queryClassesByMajorId(String id);


    /**
     * 根据班级名查询班级
     * @param name 班级名
     * @return 如果返回null,说明没有班级，反之亦然
     */
    List<Classes> queryClassesByName(String name);


    /**
     * 根据班级id和班级名查询专业
     * @param id 班级id
     * @param name 班级名
     * @return 如果返回null,说明没有这个班级，反之亦然
     */
    Classes queryClassesByIdAndName(String id, String name);

    /**
     * 保存班级信息
     * @param classes 班级对象
     * @return 如果返回-1,说明没有创建成功，反之亦然
     */
    int saveClasses(Classes classes);

    /**
     * 获取所有班级
     * @return 班级List集合
     */
    List<Classes> queryAllClasses();


}
