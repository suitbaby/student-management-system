package dao;


import pojo.Teachers;

import java.util.List;

public interface TeacherDao {

    /**
     * 根据teacher教工号密码查询teacher信息
     * @param id 教工号
     * @param password 密码
     * @return 如果返回null,说明没有这个管理员，反之亦然
     */
    Teachers queryTeacherByIdAndPassword(String id, String password);


    /**
     * 根据teacher教工号查询teacher信息
     * @param id 教工号
     * @return 如果返回null,说明没有这个管理员，反之亦然
     */
    Teachers queryTeacherById(String id);


    /**
     * 保存teacher信息
     * @param teacher 教工JavaBean对象
     * @return 返回-1表示保存失败，返回其他则保存成功
     */
    int saveTeacher(Teachers teacher);

    /**
     * 获取所有teacher信息
     * @return teacher List集合
     */
    List<Teachers> queryAllTeacher();


    /**
     * 根据所属系部 id查询 teacher
     * @param sdeptid 系部id
     * @return 系部下的所有 teacher,如果无，则返回null
     */
    List<Teachers> queryTeacherBySdeptId(String sdeptid);


    /**
     * 查询老师总数量
     * @return 返回0表示没有teacher，返回其他则表示teacher数量
     */
    int queryTeacherNumber();


    /**
     * 根据系部 id查询teacher数量
     * @param sdeptid 系部id
     * @return 返回0表示没有teacher，返回其他则表示teacher数量
     */
    int queryTeacherNumberBySdeptId(String sdeptid);

}
