package dao;

import pojo.Sdepts;

import java.util.List;

public interface SdeptDao {

    /**
     * 根据系部id查询系部
     * @param id 系部id
     * @return 如果返回null,说明没有这个用学生，反之亦然
     */
    Sdepts querySdeptById(String id);


    /**
     * 根据系部名查询系部
     * @param name 系部名
     * @return 如果返回null,说明没有这个系部，反之亦然
     */
    Sdepts querySdeptByName(String name);

    /**
     * 根据系部名查询系部
     * @param name 系部名
     * @return 如果返回null,说明没有这个系部，反之亦然
     */
    Sdepts querySdeptByIdAndName(String id, String name);

    /**
     * 保存系部信息
     * @param sdept 系部对象
     * @return 如果返回-1,说明没有创建成功，反之亦然
     */
    int saveSdept(Sdepts sdept);

    /**
     * 获取所有系部
     * @return 系部List集合
     */
    List<Sdepts> queryAllSdept();
}
