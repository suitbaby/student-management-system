package dao;

import pojo.Majors;

import java.util.List;

public interface MajorDao {
    

    /**
     * 根据专业id查询专业
     * @param id 专业id
     * @return 如果返回null,说明没有这个专业，反之亦然
     */
    Majors queryMajorById(String id);

    /**
     * 根据系部id查询专业
     * @param id 系部id
     * @return 如果返回null,说明系部下没有专业，反之亦然
     */
    List<Majors> queryMajorBySdeptId(String id);


    /**
     * 根据专业名查询专业
     * @param name 专业名
     * @return 如果返回null,说明专业集合，反之亦然
     */
    List<Majors> queryMajorByName(String name);


    /**
     * 根据专业id和专业名查询专业
     * @param id 专业id
     * @param name 专业名
     * @return 如果返回null,说明没有这个系部，反之亦然
     */
    Majors queryMajorByIdAndName(String id, String name);

    /**
     * 保存专业信息
     * @param major 专业对象
     * @return 如果返回-1,说明没有创建成功，反之亦然
     */
    int saveMajor(Majors major);

    /**
     * 获取所有专业
     * @return 专业List集合
     */
    List<Majors> queryAllMajor();
}
