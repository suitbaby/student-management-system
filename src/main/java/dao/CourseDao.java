package dao;

import pojo.Courses;

import java.util.List;

public interface CourseDao {


    /**
     * 根据课程id查询课程
     * @param id 课程id
     * @return 如果返回null,说明没有这个课程，反之亦然
     */
    Courses queryCourseById(String id);

    /**
     * 根据 teacher id查询专业
     * @param id teacher id
     * @return 如果返回null,说明teacher没课
     */
    List<Courses> queryCourseByTeacherId(String id);


    /**
     * 返回所有课程
     * @return 如果返回null,说明没有这个课程，反之亦然
     */
    List<Courses> queryAllCourse();

    /**
     * 保存课程
     * @param courses course JavaBean对象
     * @return 如果返回-1,说明失败，反之亦然
     */
    int saveCourse(Courses courses);


    /**
     * 根据课程id查找课程名
     * @param id 课程id
     * @return 课程名
     */
    String queryNameById(String id);


    /**
     * 根据课程name，teacherId，beginDate，endDate查询课程
     * @param courses 课程对象
     * @return 课程
     */
    Courses queryByNameAndTeacherId(Courses courses);
}
