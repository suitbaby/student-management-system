package dao;

import pojo.Students;

import java.util.List;

public interface StudentDao {

    /**
     * 根据学生学号查询学生信息
     * @param id 学生学号
     * @return 如果返回null,说明没有这个用学生，反之亦然
     */
    Students queryStudentById(String id);

    /**
     * 根据学生学号查询名字
     * @param id 学号
     * @return 学生名字
     */
    String queryNameById(String id);

    /**
     * 根据学生学号,密码查询学生信息
     * @param id 学生学号
     * @param password 学生密码
     * @return 如果返回null,说明没有这个学生，反之亦然
     */
    Students queryStudentByIdAndPassword(String id, String password);

    /**
     * 保存学生信息
     * @param student 学生对象
     * @return 如果返回-1,说明失败，反之亦然
     */
    int saveStudent(Students student);

    /**
     * 获取所有学生
     * @return 学生List集合
     */
    List<Students> queryAllStudent();

    /**
     * 根据calssId 获取班级下的所有学生
     * @param classId 班级id
     * @return 班级下的所有学生
     */
    List<Students> queryStudentByClassId(String classId);
}
