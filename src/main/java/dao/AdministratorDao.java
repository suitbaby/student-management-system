package dao;

import pojo.Administrators;

public interface AdministratorDao {

    /**
     * 根据管理员账号密码查询管理员信息
     * @param account 管理员账号
     * @param Password 管理员密码
     * @return 如果返回null,说明没有这个管理员，反之亦然
     */
    Administrators queryAdministratorByAccountAndPassword(String account, String Password);


    /**
     * 根据管理员账号查询管理员信息
     * @param account 管理员账号
     * @return 如果返回null,说明没有这个管理员，反之亦然
     */
    Administrators queryAdministratorByAccount(String account);


    /**
     * 保存管理员信息
     * @param administrator 管理员JavaBean对象
     * @return 返回-1表示保存失败，返回其他则保存成功
     */
    int saveAdministrator(Administrators administrator);


}
