package service;

import pojo.Tokens;

public interface TokenService {

    /**
     * 保存token
     *
     * @param token JavaBean对象
     * @return 返回-1表示添加失败，返回其他表示添加成功
     */
    int registToken(Tokens token);

    /**
     * 通过token获取 tokens
     *
     * @param token token字段
     * @return 返回tokens对象
     */
    Tokens queryByToken(String token);

    /**
     * 根据token删除
     *
     * @param token token字段
     * @return 返回-1表示删除失败，返回其他表示删除成功
     */
    int deleteByToken(String token);

    /**
     * 根据学生学号删除
     *
     * @param id 学生Id
     * @return 返回-1表示删除失败，返回其他删除成功
     */
    int deleteByStudentId(String id);

    /**
     * 根据教工号删除
     *
     * @param id 教工id
     * @return 返回-1表示删除失败，返回其他删除成功
     */
    int deleteByTeacherId(String id);

    /**
     * 根据adminId删除
     *
     * @param id admin id
     * @return 返回-1表示删除失败，返回其他删除成功
     */
    int deleteByAdminId(String id);


}
