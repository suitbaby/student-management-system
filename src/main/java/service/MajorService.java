package service;

import pojo.Majors;
import java.util.List;

public interface MajorService {

    /**
     * 创建专业
     * @param major JavaBean对象
     * @return 返回-1表示添加失败，返回其他表示添加成功
     */
    int registStudent(Majors major);

    /**
     * 返回所有专业
     * @return 专业集合
     */
    List<Majors> queryAllMajor();

    /**
     * 根据系部id返回该系下所有专业
     * @return 专业集合
     */
    List<Majors> queryAllMajorBySdeptId(String sdeptid);
}
