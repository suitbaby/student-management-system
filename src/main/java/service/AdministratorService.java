package service;

import pojo.Administrators;

/**
 * 管理员业务
 */
public interface AdministratorService {


    /**
     * 注册管理员
     * @param administrator 管理员JavaBean对象
     * @return 返回-1表示添加失败，返回其他表示添加成功
     */
    int registAdministrator(Administrators administrator);

    /**
     * 登录
     * @param account 账号
     * @param password 密码
     * @return 返回为空表示没有，返回其他表示添加成功
     */
    Administrators login(String account, String password);


    /**
     * 检查账号是否可用
     *
     * @param account 账号
     * @return 返回true表示账号已存在，返回false则表示账号可用
     */
    boolean existsAccount(String account);


}
