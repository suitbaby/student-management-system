package service;

import pojo.Teachers;
import java.util.List;

public interface TeacherService {


    /**
     * 注册老师
     * @param teacher JavaBean对象
     * @return 返回-1表示添加失败，返回其他表示添加成功
     */
    int registTeacher(Teachers teacher);


    /**
     * 登录
     * @param id 教工号
     * @param password 密码
     * @return 返回null则没有teacher对象
     */
    Teachers login(String id, String password);


    /**
     * 检查教工号是否可用
     * @param id 教工号
     * @return 返回true表示账号已存在，返回false则表示账号可用
     */
    boolean existsTeacherId(String id);


    /**
     * 查询老师总数量
     * @return 返回0表示没有teacher，返回其他则表示teacher数量
     */
    int teacherNumber();

    /**
     * 根据系部id查询系部老师数量
     * @param sdeptid 系部id
     * @return 返回0表示没有teacher，返回其他则表示teacher数量
     */
    int teacherNumberBySdeptId(String sdeptid);

    /**
     * 获取所有teacher信息
     * @return 返回所有teacher信息
     */
    List<Teachers> getAllTeacher();


    /**
     * 根据teacher id 查询teacher信息
     * @param id teacher id
     * @return teacher信息
     */
    Teachers queryTeacherById(String id);

}
