package service;

import pojo.Students;

import java.util.List;

public interface StudentService {


    /**
     * 注册学生
     *
     * @param student JavaBean对象
     * @return 返回-1表示添加失败，返回其他表示添加成功
     */
    int registStudent(Students student);


    /**
     * 登录
     *
     * @param id       学号
     * @param password 密码
     * @return 返回null则没有student对象
     */
    Students login(String id, String password);


    /**
     * 检查学号是否可用
     *
     * @param id 学号
     * @return 返回true表示账号已存在，返回false则表示账号可用
     */
    boolean existsStudentId(String id);


    /**
     * 返回所有学生
     *
     * @return 学生集合
     */
    List<Students> queryAllStudent();

    /**
     * 根据学生学号获取名字
     *
     * @param id 学生id
     * @return 学生名字
     */
    String queryNameById(String id);


    /**
     * 根据calssId 获取班级下的所有学生
     *
     * @param classId 班级id
     * @return 班级下的所有学生
     */
    List<Students> queryStudentByClassId(String classId);
}
