package service;

import pojo.Courses;
import java.util.List;

public interface CourseService {

    /**
     * 根据课程id获取课程信息
     * @param id 课程id
     * @return 课程信息
     */
    Courses queryCourseById(String id);

    /**
     * 创建课程
     * @param course JavaBean对象
     * @return 返回-1表示添加失败，返回其他表示添加成功
     */
    int registCourse(Courses course);

    /**
     * 返回所有课程
     * @return 课程集合
     */
    List<Courses> queryAllCourse();

    /**
     * 根据teacher id查询课程
     * @return 课程集合
     */
    List<Courses> queryAllCourseByTeacherId(String teacherId);


    /**
     * 根据课程号获取课程名
     * @param id 课程id
     * @return 返回课程名
     */
    String queryNameById(String id);


    /**
     * 获取courseId
     * @param course 课程信息
     * @return 返回课程id
     */
    int queryCourseId(Courses course);
}
