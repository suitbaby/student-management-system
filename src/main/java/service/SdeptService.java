package service;

import pojo.Sdepts;
import java.util.List;

public interface SdeptService {

    /**
     * 查询所有系部
     * @return 系部List
     */
    List<Sdepts> queryAllSdept();

    /**
     * 保存系部
     * @param sdepts 系部对象
     * @return 如果返回-1,说明失败，反之亦然
     */
    int SaveSdept(Sdepts sdepts);


}
