package service;

import pojo.Classes;
import java.util.List;

public interface ClassesService {

    /**
     * 创建班级
     * @param classes JavaBean对象
     * @return 返回-1表示添加失败，返回其他表示添加成功
     */
    int registClass(Classes classes);

    /**
     * 返回所有班级
     * @return 班级集合
     */
    List<Classes> queryAllClass();

    /**
     * 根据专业id返回该专业下所有班级
     * @return 班级集合
     */
    List<Classes> queryAllClassByMajorId(String majorId);

}
