package service.impl;

import dao.ClassDao;
import dao.impl.ClassDaoImpl;
import pojo.Classes;
import service.ClassesService;

import java.util.List;

public class ClassesServiceImpl implements ClassesService {

    private ClassDao classDao = new ClassDaoImpl();

    @Override
    public int registClass(Classes classes) {
        return classDao.saveClasses(classes);
    }

    @Override
    public List<Classes> queryAllClass() {
        return classDao.queryAllClasses();
    }

    @Override
    public List<Classes> queryAllClassByMajorId(String majorId) {
        return classDao.queryClassesByMajorId(majorId);
    }

}
