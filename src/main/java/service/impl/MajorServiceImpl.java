package service.impl;

import dao.MajorDao;
import dao.impl.MajorDaoImpl;
import pojo.Majors;
import service.MajorService;

import java.util.List;

public class MajorServiceImpl implements MajorService {

    private MajorDao majorDao = new MajorDaoImpl();

    @Override
    public int registStudent(Majors major) {
        return majorDao.saveMajor(major);
    }

    @Override
    public List<Majors> queryAllMajor() {
        return majorDao.queryAllMajor();
    }

    @Override
    public List<Majors> queryAllMajorBySdeptId(String sdeptid) {
        return majorDao.queryMajorBySdeptId(sdeptid);
    }

}
