package service.impl;

import dao.StudentDao;
import dao.impl.StudentDaoImpl;
import pojo.Students;
import service.StudentService;

import java.util.List;

public class StudentServiceImpl implements StudentService {

    private StudentDao studentDao = new StudentDaoImpl();

    @Override
    public int registStudent(Students student) {

        return studentDao.saveStudent(student);
    }

    @Override
    public Students login(String id, String password) {
        return studentDao.queryStudentByIdAndPassword(id, password);
    }

    @Override
    public boolean existsStudentId(String id) {
        Students student = studentDao.queryStudentById(id);
        return student != null;
    }

    @Override
    public List<Students> queryAllStudent() {
        return studentDao.queryAllStudent();
    }

    @Override
    public String queryNameById(String id) {
        return studentDao.queryNameById(id);
    }

    @Override
    public List<Students> queryStudentByClassId(String classId) {
        return studentDao.queryStudentByClassId(classId);
    }
}
