package service.impl;

import dao.TeacherDao;
import dao.impl.TeacherDaoImpl;
import pojo.Teachers;
import service.TeacherService;

import java.util.List;

public class TeacherServiceImpl implements TeacherService {

    private TeacherDao teacherDao = new TeacherDaoImpl();

    @Override
    public int registTeacher(Teachers teacher) {
        return teacherDao.saveTeacher(teacher);
    }

    @Override
    public Teachers login(String id, String password) {
        return teacherDao.queryTeacherByIdAndPassword(id, password);
    }

    @Override
    public boolean existsTeacherId(String id) {
        Teachers teacher = teacherDao.queryTeacherById(id);
        return teacher != null;
    }

    @Override
    public int teacherNumber() {
        return teacherDao.queryTeacherNumber();
    }

    @Override
    public int teacherNumberBySdeptId(String sdeptid) {
        return teacherDao.queryTeacherNumberBySdeptId(sdeptid);
    }


    @Override
    public List<Teachers> getAllTeacher() {
        return teacherDao.queryAllTeacher();
    }

    @Override
    public Teachers queryTeacherById(String id) {
        return teacherDao.queryTeacherById(id);
    }


}
