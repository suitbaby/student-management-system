package service.impl;

import dao.SdeptDao;
import dao.impl.SdeptDaoImpl;
import pojo.Sdepts;
import service.SdeptService;

import java.util.List;

public class SdeptServiceImpl implements SdeptService {
    private SdeptDao sdeptDao = new SdeptDaoImpl();

    @Override
    public List<Sdepts> queryAllSdept() {
        return sdeptDao.queryAllSdept();
    }

    @Override
    public int SaveSdept(Sdepts sdepts) {
        return sdeptDao.saveSdept(sdepts);
    }
}
