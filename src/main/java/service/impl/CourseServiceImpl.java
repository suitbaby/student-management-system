package service.impl;

import dao.CourseDao;
import dao.impl.CourseDaoImpl;
import pojo.Courses;
import service.CourseService;

import java.util.List;

public class CourseServiceImpl implements CourseService {
    private CourseDao courseDao = new CourseDaoImpl();

    @Override
    public Courses queryCourseById(String id) {
        return courseDao.queryCourseById(id);
    }

    @Override
    public int registCourse(Courses course) {
        return courseDao.saveCourse(course);
    }

    @Override
    public List<Courses> queryAllCourse() {
        return courseDao.queryAllCourse();
    }

    @Override
    public List<Courses> queryAllCourseByTeacherId(String teacherId) {
        return courseDao.queryCourseByTeacherId(teacherId);
    }

    @Override
    public String queryNameById(String id) {
        return courseDao.queryNameById(id);
    }

    @Override
    public int queryCourseId(Courses course) {
        return courseDao.queryByNameAndTeacherId(course).getId();
    }


}
