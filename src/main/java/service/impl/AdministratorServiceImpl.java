package service.impl;

import dao.AdministratorDao;
import dao.impl.AdministratorDaoImpl;
import pojo.Administrators;
import service.AdministratorService;

public class AdministratorServiceImpl implements AdministratorService {

    private final AdministratorDao administratorDao = new AdministratorDaoImpl();

    @Override
    public int registAdministrator(Administrators administrator) {
        return administratorDao.saveAdministrator(administrator);
    }

    @Override
    public Administrators login(String account, String password) {
        return administratorDao.queryAdministratorByAccountAndPassword(account, password);
    }

    @Override
    public boolean existsAccount(String account) {
        return administratorDao.queryAdministratorByAccount(account) != null;
    }

}
