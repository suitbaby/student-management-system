package service.impl;

import dao.TokenDao;
import dao.impl.TokenDaoImpl;
import pojo.Tokens;
import service.TokenService;

public class TokenServiceImpl implements TokenService {


    private TokenDao tokenDao = new TokenDaoImpl();

    @Override
    public int registToken(Tokens token) {
        return tokenDao.saveToken(token);
    }

    @Override
    public Tokens queryByToken(String token) {
        return tokenDao.queryByToken(token);
    }

    @Override
    public int deleteByToken(String token) {
        return tokenDao.deleteByToken(token);
    }

    @Override
    public int deleteByStudentId(String id) {
        return tokenDao.deleteByStudentId(id);
    }

    @Override
    public int deleteByTeacherId(String id) {
        return tokenDao.deleteByTeacherId(id);
    }

    @Override
    public int deleteByAdminId(String id) {
        return tokenDao.deleteByAdminId(id);
    }
}
