package service.impl;

import dao.ScoreDao;
import dao.impl.ScoreDaoImpl;
import pojo.Scores;
import service.ScoreService;

import java.util.List;

public class ScoreServiceImpl implements ScoreService {
    private ScoreDao scoreDao = new ScoreDaoImpl();

    @Override
    public int registScore(Scores score) {

        return scoreDao.saveScore(score);
    }

    @Override
    public List<Scores> queryAllScore() {
        return scoreDao.queryAllScore();
    }

    @Override
    public List<Scores> queryAllScoreByStudentId(String studentId) {
        return scoreDao.queryScoreByStudentId(studentId);
    }

    @Override
    public Double queryStudentScoreAVGByStudentId(String studentId) {
        return scoreDao.queryStudentScoreAVGByStudentId(studentId);
    }

    @Override
    public List<Scores> queryScoreByCourseId(String courseId) {
        return scoreDao.queryScoreByCourseId(courseId);
    }

    @Override
    public int updateGradeByScore(Scores score) {
        return scoreDao.updateGradeByStudentIdAndCourseId(score);
    }
}
