package service;

import pojo.Scores;

import java.util.List;

public interface ScoreService {

    /**
     * 添加成绩
     * @param score JavaBean对象
     * @return 返回-1表示添加失败，返回其他表示添加成功
     */
    int registScore(Scores score);

    /**
     * 返回所有学生每门课成绩
     * @return 学生每门课成绩集合
     */
    List<Scores> queryAllScore();

    /**
     * 根据学生id返回学生的所有课程成绩
     * @return 单个学生的成绩集合
     */
    List<Scores> queryAllScoreByStudentId(String studentId);


    /**
     * 根据学生id查询学生所有课程的平均成绩
     * @param studentId 学生学号
     * @return 所有课程的平均成绩
     */
    Double queryStudentScoreAVGByStudentId(String studentId);


    /**
     * 查询学习课程的所有学生成绩
     * @param courseId 课程号
     * @return 学生成绩的集合
     */
    List<Scores> queryScoreByCourseId(String courseId);


    /**
     * 根据studentId和courseId修改课程成绩
     * @param score 成绩对象
     * @return 返回-1修改失败，返回其他修改成功
     */
    int  updateGradeByScore(Scores score);

}
