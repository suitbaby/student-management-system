package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;

/**
 * 课程的JavaBean类
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class Courses {
    private int id;
    private String name;
    private int teacherId;
    private Timestamp beginDate;
    private Timestamp endDate;

    public Courses(String name, int teacherId, Timestamp beginDate, Timestamp endDate) {
        this.name = name;
        this.teacherId = teacherId;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public Courses(String name, int teacherId) {
        this.name = name;
        this.teacherId = teacherId;
    }
}
