package pojo;


public class Status {

    public static final String CODE = "code";
    public static final String MSG = "msg";
    public static final String DATA = "data";

    //成功
    public static final int OK = 200;

    //语法错误
    public static final int BAD_REQUEST = 400;

    //无权限
    public static final int UNAUTHORIZED = 401;

    //无法找到资源
    public static final int NOT_FOUND = 404;

    //服务器内部错误
    public static final int INTERNAL_SERVER_ERROR = 500;

}
