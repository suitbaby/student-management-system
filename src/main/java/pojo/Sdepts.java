package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系部的JavaBean类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sdepts {
    private int id;
    private String name;

    public Sdepts(String name) {
        this.name = name;
    }
}
