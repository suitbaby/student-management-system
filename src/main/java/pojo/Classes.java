package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 班级JavaBean类
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Classes {
    private int id;
    private String name;
    private String graduationYear;
    private int majorId;

}
