package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 专业JavaBean类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Majors {
    private int id;
    private String name;
    private int sdeptId;

    public Majors(String name, int sdeptId) {
        this.name = name;
        this.sdeptId = sdeptId;
    }

}
