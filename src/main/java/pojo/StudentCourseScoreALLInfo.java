package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentCourseScoreALLInfo {

    private int studentId;
    private String studentName;
    private int courseId;
    private String courseName;
    private double grade;
    private String majorName;
    private String sdeptName;


}
