package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Tokens {
    private String token;
    private Timestamp overTime;
    private Integer adminId;
    private Integer studentId;
    private Integer teacherId;
}
