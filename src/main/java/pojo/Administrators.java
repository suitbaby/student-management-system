package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 管理JavaBean类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Administrators {
    private int id;
    private String name;
    private String account;
    private String password;

    public Administrators(String name, String account, String password) {
        this.name = name;
        this.account = account;
        this.password = password;
    }

}