package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 学生JavaBean类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Students {
    private int id;
    private String name;
    private String password;
    private int sdeptId;
    private int majorId;
    private int classId;


}
