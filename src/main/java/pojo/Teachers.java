package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 教师JavaBean类
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Teachers {
    private int id;
    private String name;
    private String password;
    private int sdeptId;

}
