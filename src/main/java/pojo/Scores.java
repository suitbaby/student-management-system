package pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 成绩JavaBean类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Scores {
    private int id;
    private int studentId;
    private int courseId;
    private double grade;

    public Scores(int studentId, int courseId, double grade) {
        this.studentId = studentId;
        this.courseId = courseId;
        this.grade = grade;
    }

    public Scores(int studentId, int courseId) {
        this.studentId = studentId;
        this.courseId = courseId;
    }


}
