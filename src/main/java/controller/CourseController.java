package controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pojo.*;
import service.*;
import service.impl.*;
import utils.JSONUtils;
import utils.TokenUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;


@RestController
public class CourseController {

    private CourseService courseService = new CourseServiceImpl();
    private TeacherService teacherService = new TeacherServiceImpl();
    private TokenService tokenService = new TokenServiceImpl();
    private StudentService studentService = new StudentServiceImpl();
    private ScoreService scoreService = new ScoreServiceImpl();

    @PostMapping("/api/course/create")
    public void create(@RequestBody JSONObject json, HttpServletRequest request, HttpServletResponse response) throws IOException {

        JSONObject jsonObject = new JSONObject();
        String accessToken = request.getHeader("AccessToken");
        Tokens tokens = tokenService.queryByToken(accessToken);

        if (TokenUtils.decrypt(tokens)) {
            String beginDate = json.getString("beginDate");
            String endDate = json.getString("endDate");
            String name = json.getString("name");
            String teacherId = json.getString("teacherId");

            Courses courses = new Courses(name, Integer.parseInt(teacherId), new Timestamp(Long.parseLong(beginDate)), new Timestamp(Long.parseLong(endDate)));

            JSONArray classIds = json.getJSONArray("classIds");

            int t = courseService.registCourse(courses);
            int courseId = courseService.queryCourseId(courses);

            //创建每个班级的学生的成绩
            for (int i = 0; i < classIds.size(); i++) {

                Object calssId = classIds.get(i);

                //获取班级下的所有学生信息
                List<Students> students = studentService.queryStudentByClassId(calssId + "");

                //为每个学生创建课程成绩
                for (int j = 0; j < students.size(); j++) {
                    Students student = students.get(j);
                    scoreService.registScore(new Scores(student.getId(), courseId));
                }

            }

            if (t == -1) {
                jsonObject.put(Status.CODE, Status.BAD_REQUEST);
                jsonObject.put(Status.MSG, courses.getName() + "课程添加失败");
            } else {
                jsonObject.put(Status.CODE, Status.OK);
                jsonObject.put(Status.MSG, courses.getName() + "课程添加成功");
            }


        } else {
            jsonObject.put(Status.CODE, Status.UNAUTHORIZED);
            jsonObject.put(Status.MSG, "请登录");

        }
        JSONUtils.responseJson(response, jsonObject.toJSONString());

    }

    @GetMapping("/api/admin/course")
    public void adminCourse(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String accessToken = request.getHeader("AccessToken");
        Tokens tokens = tokenService.queryByToken(accessToken);
        JSONObject json1 = new JSONObject();

        if (TokenUtils.decrypt(tokens)) {
            List<Courses> coursesList = courseService.queryAllCourse();


            if (coursesList.size() > 0) {
                json1.put(Status.CODE, Status.OK);
                json1.put(Status.MSG, "所有课程查询成功");

                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < coursesList.size(); i++) {
                    Courses course = coursesList.get(i);
                    Teachers teacher = teacherService.queryTeacherById(course.getTeacherId() + "");
                    JSONObject json2 = new JSONObject();
                    json2.put("courseName", course.getName());
                    json2.put("beginDate", course.getBeginDate());
                    json2.put("endDate", course.getEndDate());
                    json2.put("teacherName", teacher.getName());
                    jsonArray.add(i, json2);
                }

                json1.put("data", jsonArray);
            } else {
                json1.put(Status.CODE, Status.BAD_REQUEST);
                json1.put(Status.MSG, "teacher id格式不正确");
            }


        } else {
            json1.put(Status.CODE, Status.UNAUTHORIZED);
            json1.put(Status.MSG, "请登录");

        }
        JSONUtils.responseJson(response, json1.toJSONString());

    }

    @GetMapping("/api/teacher/course")
    public void teacherCourse(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String accessToken = request.getHeader("AccessToken");
        Tokens tokens = tokenService.queryByToken(accessToken);
        JSONObject json1 = new JSONObject();

        if (TokenUtils.decrypt(tokens)) {

            List<Courses> coursesList = courseService.queryAllCourseByTeacherId(tokens.getTeacherId() + "");

            if (coursesList.size() > 0) {
                json1.put(Status.CODE, Status.OK);
                json1.put(Status.MSG, "teacher id为" + tokens.getTeacherId() + "下的所有课程查询成功");

                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < coursesList.size(); i++) {
                    Courses course = coursesList.get(i);
                    JSONObject json2 = new JSONObject();
                    json2.put("courseId", course.getId());
                    json2.put("courseName", course.getName());
                    json2.put("beginDate", course.getBeginDate());
                    json2.put("endDate", course.getEndDate());
                    jsonArray.add(i, json2);
                }
                json1.put("data", jsonArray);
            } else {
                json1.put(Status.CODE, Status.BAD_REQUEST);
                json1.put(Status.MSG, "teacher id格式不正确");
            }

        } else {
            json1.put(Status.CODE, Status.UNAUTHORIZED);
            json1.put(Status.MSG, "请登录");

        }

        JSONUtils.responseJson(response, json1.toJSONString());

    }

}
