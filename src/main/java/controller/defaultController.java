package controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class defaultController {


   @GetMapping("/index")
    public String goToIndex(HttpServletRequest request, HttpServletResponse response){
        return "index";
    }


}
