package controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pojo.Status;
import pojo.Teachers;
import pojo.Tokens;
import service.TeacherService;
import service.TokenService;
import service.impl.TeacherServiceImpl;
import service.impl.TokenServiceImpl;
import utils.JSONUtils;
import utils.TokenUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;


@RestController
public class TeacherController {

    private TeacherService teacherService = new TeacherServiceImpl();
    private TokenService tokenService = new TokenServiceImpl();

    /**
     * teacher登录
     */
    @PostMapping("/api/teacher/login")
    public void login(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws IOException {

        JSONObject result = new JSONObject();

        String id = jsonObject.getString("account");
        String password = jsonObject.getString("password");
        Teachers teacher = teacherService.login(id, password);

        if (teacher != null) {

            long time = System.currentTimeMillis();
            Long overTime = TokenUtils.overTime(time);
            String encryption = TokenUtils.encryption(id, time);
            //添加token
            int i = tokenService.registToken(new Tokens(encryption, new Timestamp(overTime), null, null, teacher.getId()));

            if (i != -1) {

                JSONObject jsonData = new JSONObject();
                jsonData.put("token", encryption);
                result.put(Status.CODE, Status.OK);
                result.put(Status.MSG, "登录成功");
                result.put("data", jsonData);

            } else {

                result.put(Status.CODE, Status.UNAUTHORIZED);
                result.put(Status.MSG, "格式不正确");

            }

        } else {
            result.put(Status.CODE, Status.BAD_REQUEST);
            result.put(Status.MSG, "账号或密码错误");

        }

        JSONUtils.responseJson(response, result.toJSONString());

    }


    @PostMapping("/api/logout")
    public void logout(HttpServletRequest req, HttpServletResponse response) throws IOException {
        String accessToken = req.getHeader("AccessToken");
        int i = tokenService.deleteByToken(accessToken);

        JSONObject jsonObject = new JSONObject();
        if (i == -1) {
            jsonObject.put(Status.CODE, Status.BAD_REQUEST);
            jsonObject.put(Status.MSG, "服务器错误");
        } else {
            jsonObject.put(Status.CODE, Status.OK);
            jsonObject.put(Status.MSG, "退出登录");
        }
        JSONUtils.responseJson(response, jsonObject.toJSONString());
    }

    /**
     * 根据JSON创建教工
     *
     * @param teacher 教师信息
     */
    @PostMapping("/api/teacher/create")
    public void create(@RequestBody Teachers teacher, HttpServletRequest request, HttpServletResponse response) throws IOException {

        String accessToken = request.getHeader("AccessToken");
        Tokens tokens = tokenService.queryByToken(accessToken);

        JSONObject jsonObject = new JSONObject();

        if (TokenUtils.decrypt(tokens)) {
            teacher.setPassword(teacher.getId() + "");
            int i = teacherService.registTeacher(teacher);

            if (i == -1) {
                jsonObject.put(Status.CODE, Status.BAD_REQUEST);
                jsonObject.put(Status.MSG, teacher.getId() + "教工添加失败");
            } else {
                jsonObject.put(Status.CODE, Status.OK);
                jsonObject.put(Status.MSG, teacher.getId() + "教工添加成功");
            }

        } else {
            jsonObject.put(Status.CODE, Status.UNAUTHORIZED);
            jsonObject.put(Status.MSG, "请登录");

        }
        JSONUtils.responseJson(response, jsonObject.toJSONString());

    }

    @GetMapping("/api/teacher")
    public void allTeacher(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String accessToken = request.getHeader("AccessToken");
        Tokens tokens = tokenService.queryByToken(accessToken);
        JSONObject result = new JSONObject();
        if (TokenUtils.decrypt(tokens)) {

            List<Teachers> teacherList = teacherService.getAllTeacher();

            result.put(Status.CODE, Status.OK);
            result.put(Status.MSG, "成功");
            result.put(Status.DATA, teacherList);


        } else {
            result.put(Status.CODE, Status.UNAUTHORIZED);
            result.put(Status.MSG, "请登录");

        }

        JSONUtils.responseJson(response, result.toJSONString());
    }


}
