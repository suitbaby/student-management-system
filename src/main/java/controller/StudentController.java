package controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.*;
import pojo.Status;
import pojo.Students;
import pojo.Tokens;
import service.StudentService;
import service.TokenService;
import service.impl.StudentServiceImpl;
import service.impl.TokenServiceImpl;
import utils.JSONUtils;
import utils.TokenUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

@RequestMapping("/api/student")
@RestController //不会走视图解析器，会直接返回一个字符串
public class StudentController {

    private StudentService studentService = new StudentServiceImpl();
    private TokenService tokenService = new TokenServiceImpl();

    @PostMapping("/logout")
    public void logout(HttpServletRequest req, HttpServletResponse response) throws IOException {
        String accessToken = req.getHeader("AccessToken");
        int i = tokenService.deleteByToken(accessToken);

        JSONObject jsonObject = new JSONObject();
        if (i == -1) {
            jsonObject.put(Status.CODE, Status.BAD_REQUEST);
            jsonObject.put(Status.MSG, "语法错误");
        } else {
            jsonObject.put(Status.CODE, Status.OK);
            jsonObject.put(Status.MSG, "退出登录");
        }
        JSONUtils.responseJson(response, jsonObject.toJSONString());
    }

    /**
     * 获取班级下所有学生
     */
    @GetMapping
    public void classStudent(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String accessToken = request.getHeader("AccessToken");
        Tokens tokens = tokenService.queryByToken(accessToken);
        JSONObject jsonObject = new JSONObject();

        if (TokenUtils.decrypt(tokens)) {
            String classId = request.getParameter("classId");
            List<Students> studentsList = studentService.queryStudentByClassId(classId);

            jsonObject.put(Status.CODE, Status.OK);
            jsonObject.put(Status.MSG, "查询班级所有学生成功");
            jsonObject.put("data", studentsList);

        } else {
            jsonObject.put(Status.CODE, Status.BAD_REQUEST);
            jsonObject.put(Status.MSG, "请登录");

        }

        JSONUtils.responseJson(response, jsonObject.toJSONString());

    }

    /**
     * 创建学生
     */
    @PostMapping("/create")
    public void create(@RequestBody Students student, HttpServletRequest request, HttpServletResponse response) throws IOException {

        String accessToken = request.getHeader("AccessToken");
        Tokens tokens = tokenService.queryByToken(accessToken);
        JSONObject jsonObject = new JSONObject();

        if (TokenUtils.decrypt(tokens)) {
            student.setPassword(student.getId() + "");
            int i = studentService.registStudent(student);

            if (i == -1) {
                jsonObject.put(Status.CODE, Status.BAD_REQUEST);
                jsonObject.put(Status.MSG, "学号为" + student.getId() + "的学生添加失败");
            } else {
                jsonObject.put(Status.CODE, Status.OK);
                jsonObject.put(Status.MSG, "学号为" + student.getId() + "的学生添加成功");
            }

        } else {
            jsonObject.put(Status.CODE, Status.UNAUTHORIZED);
            jsonObject.put(Status.MSG, "请登录");
        }
        JSONUtils.responseJson(response, jsonObject.toJSONString());
    }

    /**
     * student登录
     */
    @PostMapping("/login")
    public void login(@RequestBody JSONObject jsonObject, HttpServletResponse response) throws IOException {

        JSONObject result = new JSONObject();
        String id = jsonObject.getString("account");
        String password = jsonObject.getString("password");
        Students student = studentService.login(id, password);
        if (student != null) {
            long time = System.currentTimeMillis();

            Long overTime = TokenUtils.overTime(time);
            String encryption = TokenUtils.encryption(student.getId() + "", time);

            int i = tokenService.registToken(new Tokens(encryption, new Timestamp(overTime), null, student.getId(), null));

            if (i != -1) {

                JSONObject jsonData = new JSONObject();
                jsonData.put("token", encryption);
                result.put(Status.CODE, Status.OK);
                result.put(Status.MSG, "登录成功");
                result.put("data", jsonData);

            } else {

                result.put(Status.CODE, Status.BAD_REQUEST);
                result.put(Status.MSG, "语法错误");

            }

        } else {

            result.put(Status.CODE, Status.BAD_REQUEST);
            result.put(Status.MSG, "账号或密码错误");


        }
        JSONUtils.responseJson(response, result.toJSONString());

    }


}

