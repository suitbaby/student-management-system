package controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.*;
import pojo.Courses;
import pojo.Scores;
import pojo.Status;
import pojo.Tokens;
import service.CourseService;
import service.ScoreService;
import service.StudentService;
import service.TokenService;
import service.impl.CourseServiceImpl;
import service.impl.ScoreServiceImpl;
import service.impl.StudentServiceImpl;
import service.impl.TokenServiceImpl;
import utils.JSONUtils;
import utils.TokenUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RequestMapping
@RestController
public class SocreController {

    private ScoreService scoreService = new ScoreServiceImpl();
    private CourseService courseService = new CourseServiceImpl();
    private StudentService studentService = new StudentServiceImpl();
    private TokenService tokenService = new TokenServiceImpl();

    @GetMapping("/api/course/{courseId}/grade")
    public void courseStudentScore(@PathVariable int courseId, HttpServletRequest request, HttpServletResponse response) throws IOException {

        String accessToken = request.getHeader("AccessToken");
        Tokens tokens = tokenService.queryByToken(accessToken);
        JSONObject json1 = new JSONObject();
        if (TokenUtils.decrypt(tokens)) {
            List<Scores> scoresList = scoreService.queryScoreByCourseId(courseId + "");


            String coursename = courseService.queryNameById(courseId + "");
            json1.put(Status.CODE, Status.OK);
            json1.put(Status.MSG, "课程id为" + courseId + "下的所有班级查询成功");
            JSONObject json2 = new JSONObject();
            json2.put("courseId", courseId);
            json2.put("courseName", coursename);

            JSONArray jsonArray = new JSONArray();
            Scores score;
            for (int i = 0; i < scoresList.size(); i++) {

                score = scoresList.get(i);

                JSONObject json3 = new JSONObject();

                json3.put("id", score.getStudentId());

                json3.put("name", studentService.queryNameById(score.getStudentId() + ""));

                json3.put("grade", score.getGrade());
                jsonArray.add(i, json3);
            }

            json2.put("students", jsonArray);

            json1.put("data", json2);

            JSONUtils.responseJson(response, json1.toJSONString());
        } else {
            json1.put(Status.CODE, Status.UNAUTHORIZED);
            json1.put(Status.MSG, "请登录");
            JSONUtils.responseJson(response, json1.toJSONString());
        }

    }

    @PostMapping("/api/course/grade/update")
    public void addScore(@RequestBody Scores score, HttpServletRequest request, HttpServletResponse response) throws IOException {


        String accessToken = request.getHeader("AccessToken");
        Tokens tokens = tokenService.queryByToken(accessToken);
        JSONObject jsonObject = new JSONObject();
        if (TokenUtils.decrypt(tokens)) {
            int i = scoreService.updateGradeByScore(score);
            if (i == -1) {
                jsonObject.put(Status.CODE, Status.BAD_REQUEST);
                jsonObject.put(Status.MSG, "成绩录入失败");
            } else {
                jsonObject.put(Status.CODE, Status.OK);
                jsonObject.put(Status.MSG, "成绩录入成功");
            }
            JSONUtils.responseJson(response, jsonObject.toJSONString());
        } else {
            jsonObject.put(Status.CODE, Status.UNAUTHORIZED);
            jsonObject.put(Status.MSG, "请登录");
            JSONUtils.responseJson(response, jsonObject.toJSONString());
        }

    }


    @GetMapping("/api/student/grade")
    public void studentGetGrade(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String accessToken = request.getHeader("AccessToken");
        Tokens tokens = tokenService.queryByToken(accessToken);
        JSONObject json1 = new JSONObject();
        if (TokenUtils.decrypt(tokens)) {

            List<Scores> scoresList = scoreService.queryAllScoreByStudentId(tokens.getStudentId() + "");

            json1.put(Status.CODE, Status.OK);
            json1.put(Status.MSG, "获取所有成绩信息成功");
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < scoresList.size(); i++) {
                Scores score = scoresList.get(i);
                JSONObject json2 = new JSONObject();
                Courses course = courseService.queryCourseById(score.getCourseId() + "");
                System.out.println(course);
                json2.put("courseName", course.getName());
                json2.put("beginDate", course.getBeginDate().getTime());
                json2.put("endDate", course.getEndDate().getTime());
                json2.put("grade", score.getGrade());

                jsonArray.add(i, json2);
            }
            json1.put("data", jsonArray);

            System.out.println(jsonArray);

            JSONUtils.responseJson(response, json1.toJSONString());

        } else {
            json1.put(Status.CODE, Status.UNAUTHORIZED);
            json1.put(Status.MSG, "请登录");
            JSONUtils.responseJson(response, json1.toJSONString());
        }

    }

}
